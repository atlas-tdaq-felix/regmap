#ifndef REGMAP_SYMBOL_H
#define REGMAP_SYMBOL_H

#if REGMAP_VERSION < 0x0500
#include "regmap/regmap4-symbol.h"
#else
#include "regmap/regmap5-symbol.h"
#endif

#endif /* REGMAP_SYMBOL_H */
