#ifndef REGMAP5_COMMON_H
#define REGMAP5_COMMON_H

#include <sys/types.h>

#ifdef __cplusplus
extern "C" {
#endif

/* Max number entries of Groups */
#define MAX_ENTRIES_IN_GROUP 1000

u_long regmap_version();

#ifdef __cplusplus
}
#endif


#endif /* REGMAP5_COMMON_H */
