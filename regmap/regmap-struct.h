#ifndef REGMAP_STRUCT_H
#define REGMAP_STRUCT_H

#if REGMAP_VERSION < 0x0500
#include "regmap/regmap4-struct.h"
#else
#include "regmap/regmap5-struct.h"
#endif

#endif /* REGMAP_STRUCT_H */
