#ifndef REGMAP_COMMON_H
#define REGMAP_COMMON_H

#if REGMAP_VERSION < 0x0500
#include "regmap/regmap4-common.h"
#else
#include "regmap/regmap5-common.h"
#endif

#endif /* REGMAP_COMMON_H */
