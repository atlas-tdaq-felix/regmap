#ifndef REGMAP4_COMMON_H
#define REGMAP4_COMMON_H

#ifdef __cplusplus
extern "C" {
#endif

#include <sys/types.h>

/* Max number entries of Groups */
#define MAX_ENTRIES_IN_GROUP 1000

u_long regmap_version();

#ifdef __cplusplus
}
#endif


#endif /* REGMAP4_COMMON_H */
