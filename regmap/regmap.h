#ifndef REGMAP_H
#define REGMAP_H

#if REGMAP_VERSION < 0x0500
#include "regmap/regmap4.h"
#else
#include "regmap/regmap5.h"
#endif

#endif /* REGMAP_H */
