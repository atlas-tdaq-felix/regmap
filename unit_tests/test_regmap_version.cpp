#include <stdio.h>

#include <catch2/catch_test_macros.hpp>

#include "regmap/regmap.h"

TEST_CASE( "regmap version", "[regmap]" ) {

  uint64_t version = regmap_version();
  REQUIRE(version == REGMAP_VERSION);
}
