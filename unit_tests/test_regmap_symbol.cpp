#include <stdio.h>

#include <catch2/catch_test_macros.hpp>

#include "regmap/regmap.h"

TEST_CASE( "regmap symbol", "[.][regmap]" ) {

  u_long offset, board_id, gbt_ctrl, gbt_mon;

  offset = 0xFB900000;

  /* Read */
  board_id=0;
  regmap_cfg_get_option(offset, REG_GIT_COMMIT_NUMBER, &board_id);
  printf("GIT Commit number:          %lx\n", board_id);

  /* Write */
  regmap_cfg_set_option(offset, BF_STATUS_LEDS, 0xFF);

  /* Read Array */
  gbt_ctrl=0;
#if REGMAP_VERSION < 0x0500
  regmap_cfg_get_option(offset, BF_CR_TOHOST_GBT00_EGROUP0_CTRL_REVERSE_ELINKS, &gbt_ctrl);
#else
  regmap_cfg_get_option(offset, BF_ENCODING_LINK00_EGROUP0_CTRL_REVERSE_ELINKS, &gbt_ctrl);
#endif
  printf("GBT CTRL SWAP:     %lx\n", gbt_ctrl);

  gbt_mon=0;
#if REGMAP_VERSION < 0x0500
  regmap_cfg_get_option(offset, BF_CR_TOHOST_GBT00_MON_CROUTFIFO_PROG_FULL, &gbt_mon);
#endif
  printf("GBT MON FIFO FULL: %lx\n", gbt_mon);
}
