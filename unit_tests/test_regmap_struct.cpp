#include <stdio.h>

#include <catch2/catch_test_macros.hpp>

#include "regmap/regmap.h"

TEST_CASE( "regmap struct", "[.][regmap]" ) {

  flxcard_bar2_regs_t bar2;
  u_long board_id;
  u_long interrupts;
  u_long gbt_ctrl;
  u_long gbt_mon;
  u_long gbt_ic_datain;

#pragma GCC diagnostic ignored "-Wuninitialized"

  /* Read */
  board_id = bar2.BOARD_ID_TIMESTAMP;
  printf("Board ID:        %lx\n", board_id);

  /* Read bitfield */
  interrupts = bar2.GENERIC_CONSTANTS.INTERRUPTS;
  printf("Interrupts:      %lx\n", interrupts);

  /* Write */
  bar2.STATUS_LEDS = 0x00ff;

  /* Read Array */
#if REGMAP_VERSION < 0x0500
  gbt_ctrl = bar2.CR_GBT_CTRL[0].EGROUP_TOHOST[0].TOHOST.EPROC_ENA;
#else
  gbt_ctrl = 0;
#endif
  printf("GBT CTRL:        %lx\n", gbt_ctrl);

#if REGMAP_VERSION < 0x0500
  gbt_mon = bar2.CR_GBT_MON[0].TOHOST.EPATH0_ALMOST_FULL;
#else
  gbt_mon = 0;
#endif
  printf("GBT MON:         %lx\n", gbt_mon);

  /* Read Array bitfield */
#if REGMAP_VERSION < 0x0500
  gbt_ic_datain = bar2.MINI_EGROUP_CTRL[0].EC_TOHOST.ENCODING;
#else
  gbt_ic_datain = 0;
#endif
  printf("MINI_EGROUP_CTRL[0].EC_TOHOST.ENCODING:        %lx\n", gbt_ic_datain);

#pragma GCC diagnostic pop
}
