#!/bin/bash

wuppercodegen=wuppercodegen/wuppercodegen/cli.py
registers4=yaml/registers-4.14.yaml
registers5=yaml/registers-5.3.yaml

# Current Regmap, used by flxcard, ftools and elinkconfig
$wuppercodegen --version

# Regmap 4
$wuppercodegen $registers4 src/regmap-struct.h.template regmap/regmap4-struct.h
$wuppercodegen $registers4 src/regmap-symbol.h.template regmap/regmap4-symbol.h
$wuppercodegen $registers4 src/regmap-symbol.c.template src/regmap4-symbol.c
$wuppercodegen $registers4 src/regmap-symbol.yaml.template src/regmap4-symbol.yaml          # used by felix-register
$wuppercodegen $registers4 src/regbitmap-symbol.yaml.template src/regbitmap4-symbol.yaml    # used by felix-io

# Regmap 5
$wuppercodegen $registers5 src/regmap-struct.h.template regmap/regmap5-struct.h
$wuppercodegen $registers5 src/regmap-symbol.h.template regmap/regmap5-symbol.h
$wuppercodegen $registers5 src/regmap-symbol.c.template src/regmap5-symbol.c
$wuppercodegen $registers5 src/regmap-symbol.yaml.template src/regmap5-symbol.yaml          # used by felix-register
$wuppercodegen $registers5 src/regbitmap-symbol.yaml.template src/regbitmap5-symbol.yaml    # used by felix-io
