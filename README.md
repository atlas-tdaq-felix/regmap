Register Map API for c and c++
==============================

firmware-4 is on branch master
firmware-5 is on branch phase2/master

Usage
-----
This module uses git submodules for its dependencies.

run

git submodule init
git submodule update

to populate the submodules.

The following submodules are used:

wupperCodeGen to generate the code
firmware-4 for the source regmap version 4
firmware-5 for the source regmap version 5

you can checkout a different version under 4 and or 5 to use a different regmap.

Regenerate
----------
run

./build.sh

and then run the normal recompile using cmake.


Comments
--------
Mark Donszelmann, Mark.Donszelmann@cern.ch
